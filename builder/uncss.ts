export const uncssIgnore: Array<string | RegExp> = [
  /\.js-*/,
  /\.is-/,
  /\.*--active/,
  /\.*--sticky/,
  // /\.cards__item--becoming-nr-one/,
  // /\.slick*/,
  // /\.mesuring/,
  // /\.clients__slider--disabled/,
  // /\.uk-svg/,
  // /\.uk-scrollspy-inview/,
  // /\.uk-animation-fade/,
  // /\.uk-animation-slide-bottom-medium/,
  // /\.uk-animation-slide-left-medium/,
  // /\.uk-animation-slide-right-medium/,
  // /\.cs-imgtxt--blue/,
  // /\.cs-imgtxt--white/,
  // /\.cs-imgtxt--gray/,
  // /\.career__indicator--position/,
  // /\.career__info--hidden/,
];

export const uncssIgnoreSheets: Array<string | RegExp> = [
  '/fonts.googleapis/'
];
