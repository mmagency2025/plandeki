const $ = require('jquery');


/* functions */
function smallportfolioslider() {

    const sliderEl = $('.js-slider-mini');

    sliderEl.each(function () {

        const that = $(this);

        $(this).slick({
            dots: false,
            centerMode: true,
            slidesToShow: 2,
            infinite: true,
            initialSlide: 1,
            autoplay: true,
            autoplaySpeed: 5000,
        });
    });
}

export default smallportfolioslider;