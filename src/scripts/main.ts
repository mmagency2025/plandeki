const $ = require('jquery');
import { BrowserService } from './services/browser/browser.service';
export const browserService = new BrowserService()

const UIkit = require('uikit');
const Slick = require('slick-carousel/slick/slick');

import header from "../components/header/header";
import heroslider from "../components/hero/hero";
import smallportfolioslider from "../components/smallportfolio/smallportfolio";

window.addEventListener("DOMContentLoaded", () => {

  header();
  heroslider();
  smallportfolioslider()

}, { once: true });
